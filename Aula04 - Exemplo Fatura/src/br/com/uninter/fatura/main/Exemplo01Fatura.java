package br.com.uninter.fatura.main;

import br.com.uninter.fatura.modelo.Fatura;
import br.com.uninter.fatura.modelo.Produto;

public class Exemplo01Fatura {

	public static void main(String[] args) {
		Produto p1 = new Produto("TV", 
								"TV 52\"", 
								1200.99f, 
								"12/12/2012");
		
		
		Produto p2 = new Produto("Celular", 
								"Celular top", 
								5999.99f, 
								"10/12/2012");
		
		
		Produto p3 = new Produto("Top Therm", 
								"Fazedora de iogurte", 
								599.99f, "10/12/2012");

		
		
		Fatura fatura = new Fatura("Casas Bahia", "123", "31/12/2012");
		
		fatura.adicionarProduto(p1);
		fatura.adicionarProduto(p2);
		fatura.adicionarProduto(p3);
		
		
		fatura.imprime();
		
	}

}
