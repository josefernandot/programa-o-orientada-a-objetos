package br.com.uninter.fatura.modelo;

public class Fatura {
	
	//ATRIBUTOS
	private String nomeLoja;
	private String cnpj;
	private String dataVencimento;
	private Produto[] produtos; //vetor de produtos nnnnnn[] aaaaaaa
	private int numProdutos;
	
	
	
	//CONSTRUTORES
	public Fatura(String nomeLoja, String cnpj, String dataVencimento) {
		this.nomeLoja = nomeLoja;
		this.cnpj = cnpj;
		this.dataVencimento = dataVencimento;
		this.produtos = new Produto[100];
		this.numProdutos = 0;
		
	}
	
	
	
	
	//M�TODOS
	public void adicionarProduto(Produto produto){
		this.produtos[this.numProdutos] = produto;
		this.numProdutos++;
	}
	
	
	public void imprime(){
		System.out.println(" --- FATURA --- ");
		System.out.println("Loja.......: " + nomeLoja);
		System.out.println("cnpj.......: " + cnpj);
		System.out.println("vencimento.......: " + dataVencimento);
		
		for (int i = 0; i < this.numProdutos; i++) {
			Produto item = produtos[i];
			
			//System.out.println(item); // imprime o enere�o de memoria
			item.imprime();
		}
	}
	
	
	
	
	
}
