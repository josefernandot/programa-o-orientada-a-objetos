package br.com.uninter.fatura.modelo;

public class Produto {
	
	// ATRIBUTOS
	private String nome;
	private String descricao;
	private float valor;
	private String dataCompra;
	
	
	
	// CONSTRUTORES
	public Produto(String nome, String descricao, float valor, String dataCompra) {
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.dataCompra = dataCompra;
	}
	
	
	
	// M�TODOS
	public void imprime(){
		System.out.printf("%-20s: %s\n", nome, descricao);
		System.out.printf("%-20s: %.2f\n", dataCompra, valor);
	}
	
}
