package br.uninter.classes;
/**
 * 
 * @author alunoanalise
 *Esta classe representa um carro no mundo real
 */
public class Carro {

	
	//ATRIBUTOS
	private String placa;
	private String marca;
	private String cor;
	private float tanque;
	
	
	//CONSTRUTORES
	//CONSTRUTORES s�o m�todos especiais chamados na cria��o dos objetos.
	//Possuem: - mesmo nome da classe,
	// - n�o tem tipo de retorno.
	//Construtores devem inicializar os atributos
	public Carro(String placa, String marca, String cor){
		//THIS � uma referencia � pr�pria classe
		System.out.println("Criando carro " + placa);
		this.placa = placa;
		this.marca = marca;
		this.cor = cor;
		this.tanque = 0;
	}
	
	
	
	
	//M�TODOS
	public void anda(){
		if (this.tanque > 0){
		System.out.println("Carro " + placa + " andando...");
		this.tanque -= 5;
		}
		else{
			System.out.println("Sem combustivel; favor abastecer o tanque!!!");
		}
	}
	
	
	public void para(){
		System.out.println("Carro " + placa + " parado!");
	}
	
	
	public void abastece(float quantidade){
		if (quantidade > 0){
		System.out.println("Carro " + placa + " sendo abastecido!");
		
		//incrementar o tanque, o que tinha + a 'quantidade'
		this.tanque += quantidade;
		}
		else{
			System.out.println("Quantidade inv�lida!");
		}
	}
	
	
	
}
