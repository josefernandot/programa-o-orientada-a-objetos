package br.uninter.classes;

public class Exemplo01 {

	public static void main(String[] args) {
		//<NomeClasse> <variavel> = new <Construtor()>;
		Carro fusca = new Carro("ABC-1234", "VW", "Verde");
		
		fusca.anda();
		fusca.para();
		fusca.anda();
		fusca.abastece(20);
		fusca.anda();
		fusca.abastece(-20);
		fusca.anda();
		fusca.anda();

	}

}
